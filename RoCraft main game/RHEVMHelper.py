def _init_api(self):
    self.log.debug("Doing blocking acquire() on global RHEVM API connection lock")
    self.api_connections_lock.acquire()
    self.log.debug("Got global RHEVM API connection lock")
    url = self.api_details['url']
    username = self.api_details['username']
    password = self.api_details['password']
    self.api = API(url=url, username=username, password=password, insecure=False)
